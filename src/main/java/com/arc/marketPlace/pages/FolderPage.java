package com.arc.marketPlace.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.Common.utils.PropertyReader;
import com.arc.Common.utils.SkySiteUtils;
import com.arc.Common.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class FolderPage extends LoadableComponent<FolderPage>{
	
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	ProjectDashboardPage projectDashboardPage;
	
	/**
	 * Identifying web elements using FindBy annotation.
	 */
	
	@FindBy(css="#aPrjAddFolder")
	WebElement btnAddNewFolder;
	
	@FindBy(css="#txtSearchKeyword")
	WebElement txtSearchField;
	
	@FindBy(css="#btnSearch")
	WebElement btnGlobalSearch;
	
	@FindBy(css="#txtFolderName")
	WebElement txtBoxFolderName;
	
	@FindBy(css=".chkExcludedrawing")
	WebElement chkBoxExcludedrawing;
	
	@FindBy(css=".chkExcludedrawing")
	WebElement chkBoxExcludeLD;
	
	@FindBy(css="#btnFolderCreate")
	WebElement btnCreateFolder;
	
	@FindBy(css="#btnFolderCreateThisFolder")
	WebElement btnUploadFilesToFolder;
	
	@FindBy(css=".noty_text")
	WebElement notificationMsg;
	
	@FindBy(css=".sel-gridview")
	WebElement handIconSelect;
	
	@FindBy(css="#docsExport")
	WebElement btnExportToCSV;
	
	@FindBy(css=".aProjHeaderLink")
	WebElement ParentFold_BreadCrumb;
	
	@FindBy(css=".draggablefolder")
	WebElement objLDFolder;
	
	@FindBy(css=".panel-body.pw-icon-lg-grey")
	WebElement objNoLatestDocsAvailable;
	
	//Elements From File Edit window
	@FindBy(css="#txtDocName")
	WebElement txtDocName;
	
	@FindBy(css="#txtRevDate")
	WebElement txtRevisionDate;
	
	@FindBy(css="#btnEditDocAttr")
	WebElement btnSaveEditDocWindow;
	
	@FindBy(css=".btn.btn-default.btnCancel")
	WebElement btnCancelEditDocWindow;
	
	//Elements From Edit attributes window
	
	@FindBy(css="#txtImageName")
	WebElement txtPhotoName;
	
	@FindBy(xpath="//button[@class='btn btn-default btnCancel']")
	WebElement butCancelEditAttriWind;
	
	//Elements for upload and publish
	
	@FindBy(css="#aPrjUpldFile2")
	WebElement btnUploadfile;
	
	@FindBy(xpath="//i[@class='icon icon-upload-alt icon-lg']")
	WebElement btnUploadfile1;
	
	@FindBy(css="#btnKloudless")
	WebElement btnCloudAccount;
	
	@FindBy(xpath="//input[@name='qqfile']")
	WebElement btnChooseFile;
	
	@FindBy(css="#DonotIndexFiles")
	WebElement btnUploadWithoutIndex;
	
	@FindBy(css="#btnAutoOCR")
	WebElement btnUploadWithIndex;
	
	@FindBy(linkText="Process completed")
	WebElement linkProcessCompleted;
	
	//Objects related to file sendlink URL
	@FindBy(css="#aZipProgress")
	WebElement btnSingFileDownload;
	
	@FindBy(css="#aTotalZipDownload")
	WebElement btnToatalZipDownload;
	
	@FindBy(xpath="(//a[@class='preview document-preview-event'])[1]")
	WebElement thumbFirstlocatFile;
	
	//Objects related to viewer level send link
	@FindBy(css="#ddlRevisions>a")
	WebElement moreIconFileView;
	
	@FindBy(css="#aSend>a")
	WebElement tabSendLinkFileView;
	
	@FindBy(css="#txtCopyToClipboard")
	WebElement txtCopyToClipboard;
	
	@FindBy(css="#btnAddressBook")
	WebElement btnContact;
	
	@FindBy(css="#txtFileSendMessage")
	WebElement txtMessage;
	
	@FindBy(css="#btnAddContact")
	WebElement btnSelectContact;
	
	@FindBy(css="#btnSendLink")
	WebElement btnSendFileView;
	
	@FindBy(css="#aShareLinks")
	WebElement tabSendMultFile;
	
	@FindBy(css=".icon.icon-zip.pw-icon-brown.icon-lg.icon-2x")
	WebElement iconZip_AfterSelctFiles;
	
	@FindBy(css=".btn.btn-default.cancelMulSel")
	WebElement btnCanMultFileDownSelection;
	
	@FindBy(xpath="//button[@id='aTotalZipDownload' and @class='btn btn-primary hidden-xs multiFileDownload']")
	WebElement btnMultFileSelDownload;
	
	@FindBy(xpath="//*[@id='aPrjUpldFile']")
	WebElement btnMycomputer;
	
	//Elements for multiple file select and download
	@FindBy(css=".icon.icon-lg.icon-th-list")
	WebElement iconListView;
	
	@FindBy(css=".icon.icon-lg.icon-th-large")
	WebElement iconGridView;
	
	@FindBy(css="#chkFileFolderAll")
	WebElement chkboxAllFileSel;
	
	@FindBy(css="#divShowDocSelected")
	WebElement msgforselectedfiles;
	
	@FindBy(css=".dropdown.pw-sel-filefolder>a")
	WebElement btnDropdownMultFileSel;
	
	@FindBy(css="#shdFolder")
	WebElement eleSubFoldURL;
	
	@FindBy(css="#BackFolderFile")
	WebElement btnBackFold;
	
	@FindBy(css=".nav.navbar-nav.navbar-left>li>input")
	WebElement chkboxAllPhoto;
	
	@FindBy(css=".dropdown.has-tooltip>a")
	WebElement btnDropdownMultPhotoSel;
	
	@FindBy(xpath="(//button[@class='btn btn-primary btn-lg'])[2]")
	WebElement btnUploadPhotos;
	
	@FindBy(css="#btnFileUpload")
	WebElement btnFileUpload;
	
	@FindBy(xpath="//button[@class='btn btn-primary' and @data-bind='click: SaveAttributes']")
	WebElement btnSaveAttributes;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 * @return 
	 */
	public FolderPage(WebDriver driver) 
	{
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
	}
	
	
	
	/** 
	 * Method written for Add a new folder
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean New_Folder_Create(String Foldername)
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 60);
		Log.message("Waiting for Create Folder button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnAddNewFolder.click();//Clicking on Create New Folder
		Log.message("Add New folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, btnUploadFilesToFolder, 60);
		SkySiteUtils.waitTill(5000);
		txtBoxFolderName.sendKeys(Foldername);//Giving Folder Names Randomly
		Log.message("Folder Name: " + Foldername + " has been entered in Folder Name text box." );
		btnCreateFolder.click();
		Log.message("Create Folder button clicked!!!");//.noty_text
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String Msg_Folder_Create = notificationMsg.getText();
		Log.message("Notification Message after folder create is: "+Msg_Folder_Create);
		SkySiteUtils.waitTill(5000);
	//Getting Folder count after created a new folder
		int Avl_Fold_Count=0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Fold_Count = Avl_Fold_Count+1; 	
		} 
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		
		for(int j=1;j<=Avl_Fold_Count;j++)
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Exp Name:"+Foldername);
			Log.message("Act Name:"+actualFolderName);
	
		//Validating the new Folder is created or not
			if(Foldername.trim().contentEquals(actualFolderName.trim()))
			{  	
				Log.message("Folder is created successfully with name: "+Foldername);
				break;
			}
		}
		
		
		//Final Validation - Folder Create	
		if((Foldername.trim().contentEquals(actualFolderName.trim()))&&(Msg_Folder_Create.contentEquals("Folder created successfully")))
		{
			Log.message("Folder is created successfully with name: "+Foldername);
			return true;
		}
		else
		{
			Log.message("Folder creattion FAILED with name: "+Foldername);
			return false;
		}
			
	}
	
	/** 
	 * Method written for Add a new folder exclude LD
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean New_Folder_Create_ExcludeLD(String Foldername)
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 60);
		Log.message("Waiting for Create Folder button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnAddNewFolder.click();//Clicking on Create New Folder
		Log.message("Add New folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, btnUploadFilesToFolder, 60);
		SkySiteUtils.waitTill(5000);
		txtBoxFolderName.sendKeys(Foldername);//Giving Folder Names Randomly
		Log.message("Folder Name: " + Foldername + " has been entered in Folder Name text box." );
		SkySiteUtils.waitTill(3000);
		chkBoxExcludedrawing.click();
		Log.message("Clicked on checkbox of exclude LD!!!");
		SkySiteUtils.waitTill(2000);
		btnCreateFolder.click();
		Log.message("Create Folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String Msg_Folder_Create = notificationMsg.getText();
		Log.message("Notification Message after folder create is: "+Msg_Folder_Create);
		SkySiteUtils.waitTill(5000);
	//Getting Folder count after created a new folder
		int Avl_Fold_Count=0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Fold_Count = Avl_Fold_Count+1; 	
		} 
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		
		for(int j=1;j<=Avl_Fold_Count;j++)
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Exp Name:"+Foldername);
			Log.message("Act Name:"+actualFolderName);
	
		//Validating the new Folder is created or not
			if(Foldername.trim().contentEquals(actualFolderName.trim()))
			{  	
				Log.message("Folder is created successfully with name: "+Foldername);
				break;
			}
		}
		
		
		//Final Validation - Folder Create	
		if((Foldername.trim().contentEquals(actualFolderName.trim()))&&(Msg_Folder_Create.contentEquals("Folder created successfully")))
		{
			Log.message("Folder is created successfully with name: "+Foldername);
			return true;
		}
		else
		{
			Log.message("Folder creattion FAILED with name: "+Foldername);
			return false;
		}
			
	}
	
	
	/** 
	 * Method written for Select a folder
	 * Scripted By: Naresh Babu
	 */
	public void Select_Folder(String Foldername)
	{
		SkySiteUtils.waitForElement(driver, btnAddNewFolder,120);
		Log.message("Waiting for Create Folder button to be appeared");
	//Getting Folder count after created a new folder
		int Avl_Fold_Count=0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Fold_Count = Avl_Fold_Count+1; 	
		} 
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		
		for(int j=1;j<=Avl_Fold_Count;j++)
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Exp Name:"+Foldername);
			Log.message("Act Name:"+actualFolderName);
	
		//Validating the new Folder is created or not
			if(Foldername.trim().contentEquals(actualFolderName.trim()))
			{  	
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).click();//Select a folder
				Log.message("Expected Folder is clicked successfully with name: "+Foldername);
				break;
			}
		}
			
	}
	
	
	/** 
	 * Method written for Select Gallery folder
	 * Scripted By: Naresh Babu
	 */
	public void Select_Gallery_Folder()
	{
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
	//Getting Folder count after created a new folder
		int Avl_Fold_Count=0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Fold_Count = Avl_Fold_Count+1; 	
		} 
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		
		for(int j=1;j<=Avl_Fold_Count;j++)
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Act Name:"+actualFolderName);
	
		//Validating the new Folder is created or not
			if(actualFolderName.trim().contentEquals("Gallery"))
			{  	
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).click();//Select Gallery folder
				Log.message("Clicked on Gallery Folder");
				break;
			}
		}
			
	}
	
	/** 
	 * Method written for Search a folder and export
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean SearchA_Folder_AndExport(String Foldername) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 60);
		Log.message("Waiting for Create Folder button to be appeared");
		txtSearchField.sendKeys(Foldername);
		Log.message("Entered Folder Name: "+Foldername+" in search edit field.");
		btnGlobalSearch.click();
		Log.message("Clicked on Search button!!!");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, handIconSelect, 60);
		//Validate the search results
		String Search_Results = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		if((Search_Results.trim()).contentEquals(Foldername))
		{
			Log.message("Search results are proper with key of: "+Foldername);
		}

			//Calling "Deleting download folder files" method
			String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
			Log.message("Download Path is: "+Sys_Download_Path);
			
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
			SkySiteUtils.waitTill(10000);
			
			btnExportToCSV.click();
			Log.message("Clicked on Export to csv button!!!");
			SkySiteUtils.waitTill(10000);
			
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(20000);
			}
			
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).click();
			Log.message("Search Folder is selected successfully!!!");
			SkySiteUtils.waitTill(10000);
			
			//Get The Count of available files in search folder
			int Avl_File_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_File_Count = Avl_File_Count+1; 	
			} 
			Log.message("Available Folder Count is: "+Avl_File_Count);
			
			//Capturing All required validations from application
			String SubFolder_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
			String Exp_Document_Path = Foldername+" "+">>"+" "+SubFolder_Name;
			//Log.message("Exp Document Path is: "+Exp_Document_Path);
			
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).click();
			Log.message("Clicked on Sub folder to select.");
			SkySiteUtils.waitTill(7000);
			
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[1]")).click();
			Log.message("Clicked on More options icon for the 1st file.");
			SkySiteUtils.waitTill(2000);
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])[2]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click edit
			Log.message("Clicked on Edit Tab for the 1st file.");
			SkySiteUtils.waitForElement(driver, btnSaveEditDocWindow, 30);//Wait for save button of edit fields window
			String Exp_Document_Name = txtDocName.getAttribute("value");
			//Log.message("Exp Document Name is: "+Exp_Document_Name);
			String Exp_Revision_Date = txtRevisionDate.getAttribute("value");
			//Log.message("Exp_Revision_Date is: "+Exp_Revision_Date);
			String TimeSplitBy = " ";
			String[] Divide_Date = Exp_Revision_Date.split(TimeSplitBy);
			String Exp_Only_Date = Divide_Date[0];
			String Exp_Only_Time = Divide_Date[1];
			String Exp_Only_Noon = Divide_Date[2];
			//Log.message("Exp_OnlyRevision_Date is: "+Exp_Only_Date);
			//Log.message("Exp_OnlyRevision_Time is: "+Exp_Only_Time);
			//Log.message("Exp_OnlyRevision_AMorPM is: "+Exp_Only_Noon);
			
			btnCancelEditDocWindow.click();//Close the edit window
			
			//Validating the CSV file from download folder	
			String csvFileToRead = PropertyReader.getProperty("csvPath_Download_Dashboard");
			BufferedReader br = null;
			String line = null; 
			String splitBy = ",";
			int count = 0;
			String ActFileName = null;
			String ActReviDate = null;
			String ActRevNumber = null;
			String ActDocuPath = null;
			String ActPublishStatus = null;
			int Match_Count = 0;
			
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				//Log.message("Downloaded csv file have data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				ActFileName = ActValue[0];
				ActFileName = ActFileName.replace("\"", "");
				//Log.message("Document Name from csv file is: "+ ActFileName);
				ActReviDate = ActValue[1];
				ActReviDate = ActReviDate.replace("\"", "");
				//Log.message("Document Revision Date from csv file is: "+ ActReviDate);
				String[] Divide_Date_csv = ActReviDate.split(TimeSplitBy);
				String Act_Only_Date = Divide_Date_csv[0];
				String Act_Only_Time = Divide_Date_csv[1];
				//String Act_Only_Noon = Divide_Date_csv[2];
				//Log.message("Act_OnlyRevision_Date is: "+Act_Only_Date);
				//Log.message("Act_OnlyRevision_Time is: "+Act_Only_Time);
				//Log.message("Act_OnlyRevision_AMorPM is: "+Act_Only_Noon);
				ActRevNumber = ActValue[2];
				ActRevNumber = ActRevNumber.replace("\"", "");	
				//Log.message("Document Revision Number from csv file is: "+ ActRevNumber);
				ActDocuPath = ActValue[3];
				ActDocuPath = ActDocuPath.replace("\"", "");
				//Log.message("Document Path from csv file is: "+ ActDocuPath);
				ActPublishStatus = ActValue[7];
				ActPublishStatus = ActPublishStatus.replace("\"", "");
				//Log.message("Is document published or nor from csv file is: "+ ActPublishStatus);
				 
				if((ActFileName.contains(Exp_Document_Name))&&(ActRevNumber.contentEquals("1"))&&(ActPublishStatus.equalsIgnoreCase("Yes"))
						&&(Act_Only_Date.contentEquals(Exp_Only_Date))&&(Act_Only_Time.contains(Exp_Only_Time))
						&&(ActReviDate.contains(Exp_Only_Noon))&&(ActDocuPath.contentEquals(Exp_Document_Path)))
				{
					Match_Count = Match_Count+1;
				}
			}
			Log.message("Downloaded csv file have data in : "+count+ " rows");
			Avl_File_Count=Avl_File_Count+3;
		
		//Final Validation
		if((count==Avl_File_Count)&&(Match_Count == 1))
		{
			Log.message("Search Folder Exported File is having data properly!!!");
			return true;
		}
		else
		{
			Log.message("Search Folder Exported File is NOT having data properly!!!");
			return false;
		}
			
	}

	
	/** 
	 * Method written for Search a Sub folder(5th level) and export
	 * Scripted By: Naresh Babu Kavuru
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean SearchA_SubFolder_AndExport(String Foldername) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		txtSearchField.sendKeys(Foldername);
		Log.message("Entered Folder Name: "+Foldername+" in search edit field.");
		btnGlobalSearch.click();
		Log.message("Clicked on Search button!!!");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, handIconSelect, 20);
		//Validate the search results
		String Search_Results = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		if((Search_Results.trim()).contentEquals(Foldername))
		{
			Log.message("Search results are proper with key of: "+Foldername);
		}

			//Calling "Deleting download folder files" method
			String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
			Log.message("Download Path is: "+Sys_Download_Path);
			
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
			SkySiteUtils.waitTill(15000);
			
			btnExportToCSV.click();
			Log.message("Clicked on Export to csv button!!!");
			SkySiteUtils.waitTill(10000);
			
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Down PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(20000);
			}
			
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).click();
			Log.message("Search Folder is selected successfully!!!");
			SkySiteUtils.waitTill(5000);
			
			//Get The Count of available files in selected folder
			int Avl_File_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_File_Count = Avl_File_Count+1; 	
			} 
			Log.message("Available File Count is: "+Avl_File_Count);
			
			//Capturing All required validations from application
			String SubFolder_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[4]/span/a")).getText();
			String SubFolder1_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[5]/span/a")).getText();
			String SubFolder2_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[6]/span/a")).getText();
			String SubFolder3_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[7]/span/a")).getText();
			String SubFolder4_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[8]/span/a")).getText();

String Exp_Document_Path = SubFolder_Name+" >> "+SubFolder1_Name+" >> "+SubFolder2_Name+" >> "+SubFolder3_Name+" >> "+SubFolder4_Name+" >> "+Foldername;
			//Log.message("Exp Document Path is: "+Exp_Document_Path);
			
			String Exp_Document_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
			//Log.message("Exp Document Name is: "+Exp_Document_Name);
			
			//Validating the CSV file from down folder	
				String csvFileToRead = PropertyReader.getProperty("csvPath_Download_Dashboard");
				BufferedReader br = null;
				String line = null; 
				String splitBy = ",";
				int count = 0;
				String ActFileName = null;
				String ActRevNumber = null;
				String ActDocuPath = null;
				String ActPublishStatus = null;
				int Match_Count = 0;
				
				br = new BufferedReader(new FileReader(csvFileToRead)); 
				while ((line = br.readLine()) != null) 
				{  
					count=count+1;
					//Log.message("Downloaded csv file have data in : "+count+ " rows");
					String[] ActValue = line.split(splitBy);
					ActFileName = ActValue[0];
					ActFileName = ActFileName.replace("\"", "");
					//Log.message("Document Name from csv file is: "+ ActFileName);				
					ActRevNumber = ActValue[2];
					ActRevNumber = ActRevNumber.replace("\"", "");	
					//Log.message("Document Revision Number from csv file is: "+ ActRevNumber);
					ActDocuPath = ActValue[3];
					ActDocuPath = ActDocuPath.replace("\"", "");	
					//Log.message("Document Path from csv file is: "+ ActDocuPath);
					ActPublishStatus = ActValue[7];
					ActPublishStatus = ActPublishStatus.replace("\"", "");
					//Log.message("Is document published or not from csv file is: "+ ActPublishStatus);
					 
					if((ActFileName.contains(Exp_Document_Name))&&(ActRevNumber.contentEquals("1"))&&(ActPublishStatus.equalsIgnoreCase("Yes"))
							&&(ActDocuPath.contentEquals(Exp_Document_Path)))
					{
						Match_Count = Match_Count+1;
					}
				}
				Log.message("Downloaded csv file have data in : "+count+ " rows");
				Avl_File_Count=Avl_File_Count+1;
		
		//Final Validation
		if((count==Avl_File_Count)&&(Match_Count == 1))
		{
			Log.message("Search Sub Folder Exported File is having data properly!!!");
			return true;
		}
		else
		{
			Log.message("Search Sub Folder Exported File is NOT having data properly!!!");
			return false;
		}
			
	}

	
	
	/** 
	 * Method written for export a folder contains unsupported files (.jpg, excel) 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean Export_Folder_WithUnSupportFiles() throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		
	//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
			
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(20000);
			
		btnExportToCSV.click();
		Log.message("Clicked on Export to csv button!!!");
		SkySiteUtils.waitTill(10000);
			
		//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
			
		if(browserName.contains("firefox"))
		{
		//Handling Download PopUp of firefox browser using robot
			Robot robot=null;
			robot=new Robot();
				
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}

		String Foldername = PropertyReader.getProperty("Fold_UnSuportFiles");
		this.Select_Folder(Foldername);//Calling Select expected Folder
		SkySiteUtils.waitTill(5000);
			
		//Get The Count of available files in selected folder
		int Avl_File_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_File_Count = Avl_File_Count+1; 	
		} 
		Log.message("Available File Count is: "+Avl_File_Count);
			
		//Capturing All required validations from application
		String Exp_Document_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		Log.message("Exp Document Name is: "+Exp_Document_Name);
		
		//Validating the CSV file from download folder	
			String csvFileToRead = PropertyReader.getProperty("csvPath_Download_Dashboard");
			BufferedReader br = null;
			String line = null; 
			String splitBy = ",";
			int count = 0;
			String ActFileName = null;
			String ActRevNumber = null;
			String ActDocuPath = null;
			String ActPublishStatus = null;
			int Match_Count = 0;
			
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				//Log.message("Downloaded csv file have data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				ActFileName = ActValue[0];
				ActFileName = ActFileName.replace("\"", "");
				Log.message("Document Name from csv file is: "+ ActFileName);				
				ActRevNumber = ActValue[2];
				ActRevNumber = ActRevNumber.replace("\"", "");	
				Log.message("Document Revision Number from csv file is: "+ ActRevNumber);
				ActDocuPath = ActValue[3];
				ActDocuPath = ActDocuPath.replace("\"", "");	
				Log.message("Document Path from csv file is: "+ ActDocuPath);
				ActPublishStatus = ActValue[7];
				ActPublishStatus = ActPublishStatus.replace("\"", "");
				Log.message("Is document published or nor from csv file is: "+ ActPublishStatus);
				 
				if((ActFileName.contains(Exp_Document_Name))&&(ActRevNumber.contentEquals("1"))&&(ActPublishStatus.equalsIgnoreCase("Yes"))
						&&(ActDocuPath.contentEquals(Foldername)))
				{
					Match_Count = Match_Count+1;
				}
			}
			Log.message("Downloaded csv file have data in : "+count+ " rows");
			Avl_File_Count=Avl_File_Count+1;
		
		//Final Validation
		if((count==Avl_File_Count)&&(Match_Count == 1))
		{
			Log.message("Folder having unsupported files Exported File is having data properly!!!");
			return true;
		}
		else
		{
			Log.message("Folder having unsupported files Exported File is NOT having data properly!!!");
			return false;
		}
			
	}
	
	
	/** 
	 * Method written for Gallery folder export
	 * Scripted By: Naresh Babu KAVURU K
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean Gallery_Folder_Export() throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		
	//Calling "Select Gallery Folder" Method
		this.Select_Gallery_Folder();
		SkySiteUtils.waitTill(5000);
		
	//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
			
		btnExportToCSV.click();
		Log.message("Clicked on Export to csv button!!!");
		SkySiteUtils.waitTill(10000);
			
		//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
			
		if(browserName.contains("firefox"))
		{
		//Handling Download PopUp of firefox browser using robot
			Robot robot=null;
			robot=new Robot();
				
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
			
		//Get The Count of available files in search folder
		int Avl_File_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'photo-viewer preview')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_File_Count = Avl_File_Count+1; 	
		} 
		Log.message("Available Items Count in Gallery is: "+Avl_File_Count);
			
			//Capturing All required validations from application
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[5]")).click();
			Log.message("Clicked on More options icon for the 1st Photo.");
			SkySiteUtils.waitTill(2000);
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])[6]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click edit
			Log.message("Clicked on Edit Tab for the 1st Photo.");
			SkySiteUtils.waitForElement(driver, txtPhotoName, 30);//Wait for save button of edit fields window
			SkySiteUtils.waitTill(2000);
			String Exp_Photo_Name = txtPhotoName.getAttribute("value");
			//Log.message("Exp Photo Name is: "+Exp_Photo_Name);
			String Exp_Building = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[1]")).getAttribute("value");
			//Log.message("Exp Building Name is: "+Exp_Building);
			String Exp_Level = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[2]")).getAttribute("value");
			//Log.message("Exp Level Name is: "+Exp_Level);
			String Exp_Room = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[3]")).getAttribute("value");
			//Log.message("Exp room Name is: "+Exp_Room);
			String Exp_Area = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[4]")).getAttribute("value");
			//Log.message("Exp Area is: "+Exp_Area);
			String Exp_Description = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[5]")).getAttribute("value");
			//Log.message("Exp Description is: "+Exp_Description);
			
			butCancelEditAttriWind.click();//Close the edit attribute window
			
			//Validating the CSV file from download folder	
			String csvFileToRead = PropertyReader.getProperty("Gallery_Exp_Path");
			BufferedReader br = null;
			String line = null; 
			String splitBy = ",";
			int count = 0;
			String ActPhotoName = null;
			String ActBuilding = null;
			String ActBuildLevel = null;
			String ActRoom = null;
			String ActArea = null;
			String ActDescription = null;
			int Match_Count = 0;
			
			br = new BufferedReader(new FileReader(csvFileToRead)); 
			while ((line = br.readLine()) != null) 
			{  
				count=count+1;
				//Log.message("Downloaded csv file have data in : "+count+ " rows");
				String[] ActValue = line.split(splitBy);
				ActPhotoName = ActValue[0];
				//Log.message("Photo Name from csv file is: "+ ActPhotoName);			
				ActBuilding = ActValue[4];
				//Log.message("Building Name from csv file is: "+ ActBuilding);				
				ActBuildLevel = ActValue[5];	
				//Log.message("Building Level from csv file is: "+ ActBuildLevel);				
				ActRoom = ActValue[6];
				//Log.message("Room Number From csv file is: "+ ActRoom);				
				ActArea = ActValue[7];
				//Log.message("Room Area From csv file is: "+ ActArea);				
				ActDescription = ActValue[8];
				//Log.message("Description of Building from csv file is: "+ ActDescription);
				 
				if((ActPhotoName.contains(Exp_Photo_Name))&&(ActBuilding.contentEquals(Exp_Building))&&(ActBuildLevel.contains(Exp_Level))
						&&(ActRoom.contains(Exp_Room))&&(ActArea.contentEquals(Exp_Area))&&(ActDescription.contentEquals(Exp_Description)))
				{
					Match_Count = Match_Count+1;
				}
			}
			Log.message("Downloaded csv file have data in : "+count+ " rows");
			Avl_File_Count=Avl_File_Count+2;
		
		//Final Validation
		if((count==Avl_File_Count)&&(Match_Count == 1))
		{
			Log.message("Gallery Folder Exported CSV File is having data properly!!!");
			return true;
		}
		else
		{
			Log.message("Gallery Folder Exported CSV File is NOT having data properly!!!");
			return false;
		}
			
	}
	
	
	/** 
	 * Method written for Validate uploaded files from folder
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	//Validate Files are uploaded properly or not
	public boolean ValidateUploadFiles(String FolderPath,int FileCount) throws InterruptedException
	{	
		boolean result = false;
		try
		{	
			String ActFileName=null;
			String expFilename=null;
			int uploadSuccessCount=0;
			//Reading all the file names from input folder
			File[] files = new File(FolderPath).listFiles();
			
			for(File file : files)
			{
				if(file.isFile()) 
				{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);
				SkySiteUtils.waitTill(500);
				for(int n=2;n<=FileCount+1;n++)
				{											
				ActFileName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+n+"]/section[2]/h4/span[1]")).getText(); 
				Log.message("Actual File name is:"+ActFileName);
					     
					if(expFilename.trim().equalsIgnoreCase(ActFileName.trim()))
					{	    			    	 
					  uploadSuccessCount=uploadSuccessCount+1;
					  Log.message("File uploded success for:" +ActFileName+"Sucess Count:"+uploadSuccessCount);
					  break;
					}
				}
			     		     
				}
					     
			}
					     
			//Checking whether file count is equal or not		     
			if(FileCount==uploadSuccessCount)	
			{
				Log.message("All the files are available in folder.");
				result=true;
			}
			else
			{	
				Log.message("All the files are NOT available in folder.");
				result=false;
			}
			
		}//End of try block
			
		catch(Exception e)
		{	
			result=false;	
		}
			
		finally
		{
				return result;	
		}
	}

	/** 
	 * Method written for Validate uploaded Photos to gallery
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	//Validate Photos are uploaded properly or not
	public boolean ValidateUploadPhotos(String FolderPath,int FileCount) throws InterruptedException
	{
		boolean result=false;
		String ActPhotoName=null;
		String expPhotoname=null;
		int uploadSuccessCount=0;
		
		try
		{			 
		//Reading all the photo names in a folder
			File[] files = new File(FolderPath).listFiles();
		
			for(File file : files)
			{
				if(file.isFile()) 
				{
					expPhotoname=file.getName();//Getting Photo Names into a variable
					Log.message("Expected Photo name is:"+expPhotoname);
					SkySiteUtils.waitTill(1000);
					for(int n=1;n<=FileCount+1;n++)
					{			
						n=n+1;
						ActPhotoName = driver.findElement(By.xpath("html/body/div[1]/div[4]/div[2]/div/div/div/ul/li["+n+"]/section[2]/h4")).getText(); 
						Log.message("Actual Photo name is:"+ActPhotoName);
				     
						if(expPhotoname.trim().contains(ActPhotoName.trim()))
						{	    			    	 
							uploadSuccessCount=uploadSuccessCount+1;
							Log.message("File uploded success for:" +ActPhotoName+"Sucess Count:"+uploadSuccessCount);
							break;
						}
					}
		     		     
				}
				     
			}
				     
			//Checking whether file count is equal or not		     
			if(FileCount==uploadSuccessCount)	
			{
				result=true;		
			}
			else
			{	
				result=false;
			}
		
		}//End of try block
		
		catch(Exception e)
		{	
			result=false;	
		}
		
		finally
		{
			return result;	
		}
	}
	
	/** 
	 * Method written for Validate LD folder is empty or not
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean Validate_LD_FolderIsEmpty() throws InterruptedException
	{			
		SkySiteUtils.waitForElement(driver, objLDFolder, 30);
		SkySiteUtils.waitTill(2000);
		objLDFolder.click();
		Log.message("Clicked on LD folder.");
		SkySiteUtils.waitForElement(driver, objNoLatestDocsAvailable, 30);
		if(objNoLatestDocsAvailable.isDisplayed())
		{
			Log.message("LD Folder is not having any files.");
			SkySiteUtils.waitTill(5000);
			ParentFold_BreadCrumb.click();
			Log.message("Clicked on project header from breadcrum.");
			return true;
		}
		else
		{
			Log.message("Still LD Folder is having files after select exclude LD.");
			return false;
		
		}
	}
	
	/** 
	 * Method written for upload files using without index
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean UploadFiles_WithoutIndex(String FolderPath, int FileCount) throws InterruptedException, AWTException, IOException
	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnUploadfile.click();//Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();//Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(15000);

	//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;		
		randomFileName rn=new randomFileName();
		String tmpFileName="c:/"+rn.nextFileName()+".txt";							
		output = new BufferedWriter(new FileWriter(tmpFileName,true));					
		String expFilename=null;
		File[] files = new File(FolderPath).listFiles();				
		for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);	
			}
		}

		output.flush();
		output.close();
					
	//Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+ FolderPath+" "+tmpFileName );
		Log.message("AutoIT Script Executed!!");			
		SkySiteUtils.waitTill(10000);
		
		SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
	//Delete the temp file
		try
		{
			File file = new File(tmpFileName);
			if(file.delete()){
			Log.message(file.getName() + " is deleted!");
			}else{
				Log.message("Delete operation is failed.");
				}
		}
		catch(Exception e)
		{
			Log.message("Exception occured!!!"+e);	
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithoutIndex.click();//Click on Upload without index  
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 180);
		Log.message("Waiting for Notification Message to be appeared");
		String UploadSuccess_Message = notificationMsg.getText();
		Log.message("Notification Message after upload is: "+UploadSuccess_Message);

		SkySiteUtils.waitForElement(driver, linkProcessCompleted, 180);
		linkProcessCompleted.click();
		Log.message("Clicked on Process Completed Link.");
		SkySiteUtils.waitTill(10000);
		
		result = this.ValidateUploadFiles(FolderPath, FileCount);//Calling validate files method
		
		if(result==true)
		{
			driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();//Click on project header to navigate to folder level
			SkySiteUtils.waitTill(10000);
			Log.message("Clicked on project header.");
			Log.message("Upload files using donot index is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Upload files using donot index is not working!!!");
			return false;
		}		
		
	}
	
	public boolean UploadFiles_WithoutIndex1(String FolderPath, int FileCount) throws InterruptedException, AWTException, IOException
	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnUploadfile.click();//Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();//Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(15000);

	//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;		
		randomFileName rn=new randomFileName();
		String tmpFileName="c:/"+rn.nextFileName()+".txt";							
		output = new BufferedWriter(new FileWriter(tmpFileName,true));					
		String expFilename=null;
		File[] files = new File(FolderPath).listFiles();				
		for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);	
			}
		}

		output.flush();
		output.close();
					
	//Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+ FolderPath+" "+tmpFileName );
		Log.message("AutoIT Script Executed!!");			
		SkySiteUtils.waitTill(10000);
		
		SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
	//Delete the temp file
		try
		{
			File file = new File(tmpFileName);
			if(file.delete()){
			Log.message(file.getName() + " is deleted!");
			}else{
				Log.message("Delete operation is failed.");
				}
		}
		catch(Exception e)
		{
			Log.message("Exception occured!!!"+e);	
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithoutIndex.click();//Click on Upload without index  
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 180);
		Log.message("Waiting for Notification Message to be appeared");
		String UploadSuccess_Message = notificationMsg.getText();
		Log.message("Notification Message after upload is: "+UploadSuccess_Message);

		SkySiteUtils.waitForElement(driver, linkProcessCompleted, 180);
		linkProcessCompleted.click();
		Log.message("Clicked on Process Completed Link.");
		SkySiteUtils.waitTill(10000);
		
		result = this.ValidateUploadFiles(FolderPath, FileCount);//Calling validate files method
		
		if(result==true)
		{
			//driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();//Click on project header to navigate to folder level
			SkySiteUtils.waitTill(10000);
			Log.message("Clicked on project header.");
			Log.message("Upload files using donot index is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Upload files using donot index is not working!!!");
			return false;
		}		
		
	}
	
	public boolean UploadFiles_WithoutIndex_Mycomputer(String FolderPath, int FileCount) throws InterruptedException, AWTException, IOException
	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnUploadfile1, 60);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnUploadfile1.click();//Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitTill(5000);
		btnMycomputer.click();
		Log.message("Clicked on Mycomputer button.");
		SkySiteUtils.waitForElement(driver, btnChooseFile, 30);
		Log.message("Waiting for Choose button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();//Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(25000);

	//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;		
		randomFileName rn=new randomFileName();
		String tmpFileName="c:/"+rn.nextFileName()+".txt";							
		output = new BufferedWriter(new FileWriter(tmpFileName,true));					
		String expFilename=null;
		File[] files = new File(FolderPath).listFiles();				
		for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);	
			}
		}

		output.flush();
		output.close();
		SkySiteUtils.waitTill(10000);		
	//Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+ FolderPath+" "+tmpFileName );
		Log.message("AutoIT Script Executed!!");			
		SkySiteUtils.waitTill(20000);
		
		SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 60);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
	//Delete the temp file
		try
		{
			File file = new File(tmpFileName);
			if(file.delete()){
			Log.message(file.getName() + " is deleted!");
			}else{
				Log.message("Delete operation is failed.");
				}
		}
		catch(Exception e)
		{
			Log.message("Exception occured!!!"+e);	
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithoutIndex.click();//Click on Upload without index  
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 180);
		Log.message("Waiting for Notification Message to be appeared");
		String UploadSuccess_Message = notificationMsg.getText();
		Log.message("Notification Message after upload is: "+UploadSuccess_Message);

		SkySiteUtils.waitForElement(driver, linkProcessCompleted, 180);
		linkProcessCompleted.click();
		Log.message("Clicked on Process Completed Link.");
		SkySiteUtils.waitTill(10000);
		
		result = this.ValidateUploadFiles(FolderPath, FileCount);//Calling validate files method
		
		if(result==true)
		{
			//driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();//Click on project header to navigate to folder level
			SkySiteUtils.waitTill(10000);
			Log.message("Clicked on project header.");
			Log.message("Upload files using donot index is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Upload files using donot index is not working!!!");
			return false;
		}		
		
	}
	
	/** 
	 * Method written for upload photos in gallery
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	@SuppressWarnings("deprecation")
	public boolean UploadPhotos_InGallery(String FolderPath, int FileCount) throws InterruptedException, AWTException, IOException
	{
		boolean result = false;
		SkySiteUtils.waitForElement(driver, btnUploadPhotos, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnUploadPhotos.click();//Click on Upload photos
		Log.message("Clicked on upload photos button.");
		SkySiteUtils.waitForElement(driver, btnChooseFile, 30);
		Log.message("Waiting for choose file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();//Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(10000);

	//Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;		
		randomFileName rn=new randomFileName();
		String tmpFileName="c:/"+rn.nextFileName()+".txt";							
		output = new BufferedWriter(new FileWriter(tmpFileName,true));					
		String expFilename=null;
		File[] files = new File(FolderPath).listFiles();				
		for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);	
			}
		}

		output.flush();
		output.close();
					
	//Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+ FolderPath+" "+tmpFileName );
		Log.message("AutoIT Script Executed!!");			
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnFileUpload, 30);
		Log.message("Waiting for File Upload button to be appeared");
		SkySiteUtils.waitTill(10000);
	//Delete the temp file
		try
		{
			File file = new File(tmpFileName);
			if(file.delete()){
			Log.message(file.getName() + " is deleted!");
			}else{
				Log.message("Delete operation is failed.");
				}
		}
		catch(Exception e)
		{
			Log.message("Exception occured!!!"+e);	
		}
		SkySiteUtils.waitTill(5000);
		btnFileUpload.click();//Click on Upload button  
		Log.message("Clicked on Upload button.");
		SkySiteUtils.waitForElement(driver, btnSaveAttributes, 180);
		Log.message("Waiting for Save attributes button to be appeared.");
		SkySiteUtils.waitTill(10000);
		
		//Validations on Add Attributes page
		String Photo_Information = driver.findElement(By.xpath("//h4[@class='pull-left']")).getText();//Getting count
		Log.message(Photo_Information);
		//Split the line and get the count
		Splitter SPLITTER = Splitter.on(CharMatcher.whitespace()).omitEmptyStrings();
		List<String> elements = Lists.newArrayList(SPLITTER.split(Photo_Information));					
		// Getting count - output
		String OnlyCount = elements.get(3);
		Log.message("The required count value is: "+OnlyCount);	
	//Convert String to int
		int PhotosCount=Integer.parseInt(OnlyCount);	
		int PageCount = PhotosCount/10;
		int Extra_Photo = PhotosCount%10;
		int totalPageCount=0;
				
		//Getting Page count
		if((PageCount>0)&&(Extra_Photo>0))//Multiple of 10 plus extra
		{     
			totalPageCount=PageCount+1;  		
		}
			
		if((PageCount>0)&&(Extra_Photo==0))//Multiple of 10 only
		{	
					totalPageCount=PageCount;   		
		}

		if((PageCount==0)&&(Extra_Photo<10))//less than  10 only
		{
			totalPageCount=1;	   		
		}	
			
		Log.message("Total Page Count is:"+totalPageCount); 
					
		for(int i = 1;i<=totalPageCount;i++)
		{
			if((PhotosCount<10) && (PhotosCount==FileCount))
			{
				//Fill the Attributes and use the copy down
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).sendKeys("ARC");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[1]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).sendKeys("7th_Leve");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[2]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).sendKeys("Room_3");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[3]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).sendKeys("SectorV");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[4]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).sendKeys("Kolkata_Branch");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[5]")).click();
				SkySiteUtils.waitTill(10000);
				driver.findElement(By.xpath(".//*[@id='divAttrContainer']/div[1]/div[2]/div/div[2]/button")).click();
				SkySiteUtils.waitTill(5000);							
							
			}
			if((PhotosCount>=10) && (PhotosCount==FileCount))
			{
				//Fill the Attributes and use the copy down
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).sendKeys("ARC");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[1]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).sendKeys("7th_Leve");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[2]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).sendKeys("Room_3");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[3]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).sendKeys("SectorV");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[4]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).sendKeys("Kolkata_Branch");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[5]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath(".//*[@id='divAttrContainer']/div[1]/div[2]/div/div[2]/button")).click();
				SkySiteUtils.waitTill(5000);						
			}
								
		}//end of i for loop
		SkySiteUtils.waitTill(10000);	
		
		//Calling Upload Photo validation script
		result = this.ValidateUploadPhotos(FolderPath, FileCount);//Calling validate photos method
		
		if(result==true)
		{
			Log.message("Photos Upload is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Photos Upload is not working!!!");
			return false;
		}		
		
	}
	
	
	/** 
	 * Method written for Validate Download by select multiple files
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
		
	//Validate Download by select multiple files
	public boolean ValidateDownload_BySelectMultFiles(String Foldername) throws InterruptedException, AWTException
	{		
			boolean result = false;
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, iconListView, 30);
			Log.message("Waiting for List View button to be appeared");
			SkySiteUtils.waitTill(5000);
			iconListView.click();
			Log.message("Clicked on List View button.");
			SkySiteUtils.waitForElement(driver, chkboxAllFileSel, 30);
			Log.message("Waiting for select all files checkbox to be appeared");
			//Counting number of files available
			int Avl_File_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'chkElements')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_File_Count = Avl_File_Count+1; 	
			} 
			Log.message("Availble number of files in folder is: "+Avl_File_Count);
			SkySiteUtils.waitTill(10000);
			//Select first two files from the list
			if(Avl_File_Count>=2)
			{
				for(int i=1;i<=2;i++)
				{
					//Log.message("Select a checkbox");
					driver.findElement(By.xpath("(//input[@class='chkElements chkfileelements'])["+i+"]")).click();
				}
			}
			SkySiteUtils.waitTill(5000);
			
			//Calling "Deleting download folder files" method
			String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
			Log.message("Download Path is: "+Sys_Download_Path);
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
			SkySiteUtils.waitTill(10000);
			
			String Msg_No_Of_FilesSel = msgforselectedfiles.getText();
			btnDropdownMultFileSel.click();
			SkySiteUtils.waitTill(5000);
			/*WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])[2]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click edit*/
			driver.findElement(By.xpath("//a[@id='aDownload']")).click();
			Log.message("Clicked on Download selected documents.");
			SkySiteUtils.waitTill(60000);
			
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(60000);
			}
			
			//After Download, checking whether Downloaded file is existed under download folder or not 
			String ActualFilename=null;
			File[] files = new File(Sys_Download_Path).listFiles();
						
			for(File file : files)
			{
				if(file.isFile()) 
				{
					ActualFilename=file.getName();//Getting File Names into a variable
					Log.message("Actual File name is:"+ActualFilename);
					SkySiteUtils.waitTill(1000);	
					Long ActualFileSize=file.length();
					Log.message("Actual File size is:"+ActualFileSize);
					if((ActualFilename.contains("2-files-"+Foldername))&&(ActualFileSize!=0))						
					{
						Log.message("Downloaded ZIP file is available in downloads!!!");
						result = true;
						break;
					}
								
				}
							
			}
			
			iconGridView.click();//Click on grid view
			SkySiteUtils.waitTill(5000);
			if(result==true)
				return true;
			else		
				return false;	
	}
	
	/** 
	 * Method written for Validate Download single file from file send link url
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	//Download Single File from send link URL Page - Use for file send link
	public boolean Download_SingleFile_SendLinkForAFIle(String FileName) throws InterruptedException, AWTException
	{
		boolean result=false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
		Log.message("Waiting for download button to be appeared from sendlink URL");
		String Act_FileName = driver.findElement(By.xpath("(//div[1]/h4)[2]")).getText();		
		Log.message("File from url is: "+Act_FileName);
		
		//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
		
		if(Act_FileName!=null)
		{
			btnSingFileDownload.click();//Click on Download file
			Log.message("Clicked on single file download button.");
			SkySiteUtils.waitTill(20000);
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}
			
			//After Download, checking whether Downloaded file is existed under download folder or not 
			String ActualFilename=null;
			File[] files = new File(Sys_Download_Path).listFiles();
						
			for(File file : files)
			{
				if(file.isFile()) 
				{
					ActualFilename=file.getName();//Getting File Names into a variable
					Log.message("Actual File name is:"+ActualFilename);
					SkySiteUtils.waitTill(1000);	
					Long ActualFileSize=file.length();
					Log.message("Actual File size is:"+ActualFileSize);
					if((ActualFilename.contains(FileName))&&(ActualFileSize!=0))						
					{
						Log.message("Downloaded file is available in downloads!!!");
						result = true;
						break;
					}
								
				}
							
			}
		}
		
		if(result==true)
			return true;
		else
			return false;
	
	}
	
	/** 
	 * Method written for Validate Download single file under sub folder from send link url
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	//Download Single File from send link URL Page - Use for file send link
	public boolean Download_SingleFile_SendLinkSubFold(String FileName) throws InterruptedException, AWTException
	{
		boolean result=false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
		Log.message("Waiting for download button to be appeared from sendlink URL");
		String Act_FileName = driver.findElement(By.xpath(".//*[@id='sharedDocsFolderList']/li[1]/div/div[1]/h4")).getText();		
		Log.message("File from url is: "+Act_FileName);
		
		//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
		
		if(Act_FileName!=null)
		{
			//btnSingFileDownload.click();//Click on Download file
			driver.findElement(By.xpath("(//button[@id='downldFile'])[1]")).click();
			Log.message("Clicked on single file download button.");
			SkySiteUtils.waitTill(20000);
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}
			
			//After Download, checking whether Downloaded file is existed under download folder or not 
			String ActualFilename=null;
			File[] files = new File(Sys_Download_Path).listFiles();
						
			for(File file : files)
			{
				if(file.isFile()) 
				{
					ActualFilename=file.getName();//Getting File Names into a variable
					Log.message("Actual File name is:"+ActualFilename);
					SkySiteUtils.waitTill(1000);	
					Long ActualFileSize=file.length();
					Log.message("Actual File size is:"+ActualFileSize);
					if((ActualFilename.contains(FileName))&&(ActualFileSize!=0))						
					{
						Log.message("Downloaded file is available in downloads!!!");
						result = true;
						break;
					}
								
				}
							
			}
		}
		
		if(result==true)
			return true;
		else
			return false;
	
	}
	
	
	/** 
	 * Method written for Validate Download Sub Folder from Folder send link url
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	//Download Single File from send link URL Page - Use for file send link
	public boolean Download_SubFolder_FoldSendLink(String Sub_Fold_Name) throws InterruptedException, AWTException
	{
		boolean result=false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
		Log.message("Waiting for download button to be appeared from sendlink URL");
		String Sub_FoldName_URL = driver.findElement(By.xpath("//a[@id='shdFolder']")).getText();		
		Log.message("Sub Folder from url is: "+Sub_FoldName_URL);
		
		//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
		
		if(Sub_FoldName_URL!=null)
		{
			btnSingFileDownload.click();//Click on Download file
			Log.message("Clicked on Sub Folder download button.");
			SkySiteUtils.waitTill(30000);
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}
			
			//After Download, checking whether Downloaded file is existed under download folder or not 
			String ActualFilename=null;
			File[] files = new File(Sys_Download_Path).listFiles();
						
			for(File file : files)
			{
				if(file.isFile()) 
				{
					ActualFilename=file.getName();//Getting File Names into a variable
					Log.message("Actual File name is:"+ActualFilename);
					SkySiteUtils.waitTill(1000);	
					Long ActualFileSize=file.length();
					Log.message("Actual File size is:"+ActualFileSize);
					if((ActualFilename.contains(Sub_Fold_Name))&&(ActualFileSize!=0))						
					{
						Log.message("Downloaded Subfolder Zip File available in downloads!!!");
						result = true;
						break;
					}
								
				}
							
			}
		}
		
		if(result==true)
			return true;
		else
			return false;
	
	}
	
	/** 
	 * Method written for Validate Direct Zip download for multiple file send link url
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	//Download total zip File from send link URL Page 
	public boolean Download_ZipFile_SendLinkForMultFIle(String Exp_Zip_Name) throws InterruptedException, AWTException
	{
		boolean result=false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
		Log.message("Waiting for download button to be appeared from sendlink URL");
		String Act_FileName = driver.findElement(By.xpath("(//div[1]/h4)[2]")).getText();		
		Log.message("File from url is: "+Act_FileName);
		
		//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
		
		if(Act_FileName!=null)
		{
			btnToatalZipDownload.click();//Click on Download file
			Log.message("Clicked on total zip download button.");
			SkySiteUtils.waitTill(40000);
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(40000);
			}
			
			//After Download, checking whether Downloaded file is existed under download folder or not 
			String ActualFilename=null;
			File[] files = new File(Sys_Download_Path).listFiles();
						
			for(File file : files)
			{
				if(file.isFile()) 
				{
					ActualFilename=file.getName();//Getting File Names into a variable
					Log.message("Actual File name is:"+ActualFilename);
					SkySiteUtils.waitTill(1000);	
					Long ActualFileSize=file.length();
					Log.message("Actual File size is:"+ActualFileSize);
					if((ActualFilename.contains(Exp_Zip_Name))&&(ActualFileSize!=0))						
					{
						Log.message("Downloaded Zip file is available in downloads!!!");
						result = true;
						break;
					}
								
				}
							
			}
		}
		
		if(result==true)
			return true;
		else
			return false;
	
	}
	
	
	/** 
	 * Method written for Validate Zip download for multiple file select from send link url
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	//Download zip File by select more than one file from send link URL Page 
	public boolean Download_ZipFile_SendLinkBySelectMultFIle(String Exp_Zip_Name) throws InterruptedException, AWTException
	{
		boolean result=false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
		Log.message("Waiting for download button to be appeared from sendlink URL");
		
		//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
		
		//Counting number of files available
		int Avl_File_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@type, 'checkbox')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_File_Count = Avl_File_Count+1; 	
		} 
		Log.message("Availble number of files in url is: "+Avl_File_Count);
		if(Avl_File_Count>=1)
		{
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//input[@type='checkbox'])[1]")).click();//Select 1st checkbox
			driver.findElement(By.xpath("(//input[@type='checkbox'])[2]")).click();//Select 2nd checkbox
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, btnCanMultFileDownSelection, 20);
			
			SkySiteUtils.waitTill(5000);
			btnMultFileSelDownload.click();//Click on Multiple file zip Download
			Log.message("Clicked on total zip download button.");
			SkySiteUtils.waitTill(40000);
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(40000);
			}
			
			//After Download, checking whether Downloaded file is existed under download folder or not 
			String ActualFilename=null;
			File[] files = new File(Sys_Download_Path).listFiles();
						
			for(File file : files)
			{
				if(file.isFile()) 
				{
					ActualFilename=file.getName();//Getting File Names into a variable
					Log.message("Actual File name is:"+ActualFilename);
					SkySiteUtils.waitTill(1000);	
					Long ActualFileSize=file.length();
					Log.message("Actual File size is:"+ActualFileSize);
					if((ActualFilename.contains(Exp_Zip_Name))&&(ActualFileSize!=0))						
					{
						Log.message("Downloaded Multiple file Zip is available in downloads!!!");
						result = true;
						break;
					}
								
				}
							
			}
		}
		
		if(result==true)
			return true;
		else
			return false;
	
	}
	
	
	/** 
	 * Method written for Validate Download for viewer level send link
	 *  Scipted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean ValidateDownload_ViewerLevel_SendLink() throws InterruptedException, AWTException
	{		
			boolean result = false;
			SkySiteUtils.waitTill(5000);

			//Counting number of files available
			int Avl_File_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'chkElements')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_File_Count = Avl_File_Count+1; 	
			} 
			Log.message("Availble number of files in folder is: "+Avl_File_Count);
			if(Avl_File_Count>=1)
			{
				String Exp_File_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
				Log.message("First located file is: "+Exp_File_Name);
				thumbFirstlocatFile.click();//Select a file to view
				SkySiteUtils.waitTill(10000);
				String winHandleBefore = driver.getWindowHandle();
				for(String winHandle : driver.getWindowHandles())
				{
					  driver.switchTo().window(winHandle);
				}
				SkySiteUtils.waitForElement(driver, moreIconFileView, 30);
				Log.message("Waiting for List View button to be appeared in file view");
				SkySiteUtils.waitTill(10000);
				moreIconFileView.click();
				Log.message("Clicked on more option.");
				SkySiteUtils.waitTill(3000);
				tabSendLinkFileView.click();
				Log.message("Clicked on Send link tab.");
				SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
				Log.message("Waiting for send link window");
				SkySiteUtils.waitTill(5000);
				String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");//Copy the URL
				Log.message("Generated Send Link is: "+Act_SendLinkURL);
				btnContact.click();
				Log.message("Clicked on contact-address book.");
				SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
				Log.message("Waiting for Select Contact window");
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();//Select 1st contact check box
				btnSelectContact.click();//Click on select button
				SkySiteUtils.waitTill(5000);
				txtMessage.click();
				txtMessage.sendKeys("Testing from QA_Team @Automation");
				btnSendFileView.click();//Click on send
				SkySiteUtils.waitForElement(driver, notificationMsg, 30);
				Log.message("Waiting for notify message could display");
				String NotMsg_SendLink = notificationMsg.getText();
				SkySiteUtils.waitTill(5000);
				if(NotMsg_SendLink.contentEquals("Link successfully sent"))
				{
					Log.message("Viewer level sendlink is done successfully");
					driver.close();
					SkySiteUtils.waitTill(10000);
					driver.switchTo().window(winHandleBefore);//Navigate back to parent window
					SkySiteUtils.waitTill(10000);
					driver.get(Act_SendLinkURL);//Launch the URL copied from Clip board
					SkySiteUtils.waitTill(5000);
					
					result = this.Download_SingleFile_SendLinkForAFIle(Exp_File_Name);//Validating download a file from URL
				}
					
			}
			
			if(result==true)
				return true;
			else
				return false;
			
	}
	
	
	/** 
	 * Method written for Validate select multiple files and send link and download
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	//Validate Send link by select multiple files and download
	public boolean ValidateDownload_FromSendLink_BySelectMultFiles(String Foldername) throws InterruptedException, AWTException
	{		
			boolean result1 = false;
			boolean result2 = false;
			boolean result3 = false;
			
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, iconListView, 30);
			Log.message("Waiting for List View button to be appeared");
			SkySiteUtils.waitTill(5000);
			iconListView.click();
			Log.message("Clicked on List View button.");
			SkySiteUtils.waitForElement(driver, chkboxAllFileSel, 30);
			Log.message("Waiting for select all files checkbox to be appeared");
			//Counting number of files available
			int Avl_File_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'chkElements')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_File_Count = Avl_File_Count+1; 	
			} 
			Log.message("Availble number of files in folder is: "+Avl_File_Count);
			SkySiteUtils.waitTill(10000);
			//Select first two files from the list
			if(Avl_File_Count>=2)
			{
				for(int i=1;i<=2;i++)
				{
					//Log.message("Select a checkbox");
					driver.findElement(By.xpath("(//input[@class='chkElements chkfileelements'])["+i+"]")).click();
				}
			}
			SkySiteUtils.waitTill(5000);
			
			//String Msg_No_Of_FilesSel = msgforselectedfiles.getText();
			btnDropdownMultFileSel.click();
			SkySiteUtils.waitTill(5000);
			tabSendMultFile.click();
			Log.message("Clicked on send link tab.");
			SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
			Log.message("Waiting for send link window");
			SkySiteUtils.waitTill(5000);
			String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");//Copy the URL
			Log.message("Generated Send Link is: "+Act_SendLinkURL);
			btnContact.click();
			Log.message("Clicked on contact-address book.");
			SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
			Log.message("Waiting for Select Contact window");
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();//Select 1st contact check box
			btnSelectContact.click();//Click on select button
			SkySiteUtils.waitTill(5000);
			txtMessage.click();
			txtMessage.sendKeys("Testing from QA_Team @Automation");
			btnSendFileView.click();//Click on send
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			Log.message("Waiting for notify message could display");
			String NotMsg_SendLink = notificationMsg.getText();
			SkySiteUtils.waitTill(5000);
			iconGridView.click();//Click on grid view
			SkySiteUtils.waitTill(5000);
			if(NotMsg_SendLink.contentEquals("Link successfully sent"))
			{
				Log.message("Multiple file select and sendlink is done successfully");
				SkySiteUtils.waitTill(10000);
				
				driver.get(Act_SendLinkURL);//Launch the URL copied from Clip board
				SkySiteUtils.waitTill(5000);
				SkySiteUtils.waitForElement(driver, btnSingFileDownload, 50);
				Log.message("Waiting for download button to be appeared from sendlink URL");
				
				//Single File download from send link url
				String Exp_File_Name = driver.findElement(By.xpath("(//div[1]/h4)[2]")).getText();		
				Log.message("First File from url is: "+Exp_File_Name);
				result1 = this.Download_SingleFile_SendLinkForAFIle(Exp_File_Name);//Validating download a file from URL
				
				//Total Zip download from send link url
				String Exp_Zip_Name = "2-files-"+Foldername;
				Log.message("Expected Zip is: "+Exp_Zip_Name);
				result2 = this.Download_ZipFile_SendLinkForMultFIle(Exp_Zip_Name);//Validate total zip download
				
				//Select more than one file and download the zip
				result3 = this.Download_ZipFile_SendLinkBySelectMultFIle(Exp_Zip_Name);//Validate multiple file zip download
					
			}
		
		if((result1==true)&&(result2==true)&&(result3==true))
		{
			Log.message("Multiple file send link and all types of downloads from url is working.");
			return true;
		}
		else
		{
			Log.message("Multiple file send link and all types of downloads from url is NOT working.");
			return false;
		}
		
	}
	
	
	/** 
	 * Method written for Validate select multiple photos and send link and download
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	//Validate Send link by select multiple files and download
	public boolean ValidateDownload_FromSendLink_BySelectMultPhotos(String Foldername,String Project_Name) throws InterruptedException, AWTException
	{		
			boolean result1 = false;
			boolean result2 = false;
			boolean result3 = false;
			
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, iconListView, 30);
			Log.message("Waiting for List View button to be appeared");
			SkySiteUtils.waitTill(5000);
			iconListView.click();
			Log.message("Clicked on List View button.");
			SkySiteUtils.waitForElement(driver, chkboxAllPhoto, 30);
			Log.message("Waiting for select all photos checkbox to be appeared");
			//Counting number of files available
			int Avl_File_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'media navbar-collapse pw-photo')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_File_Count = Avl_File_Count+1; 	
			} 
			Log.message("Availble number of photos in folder is: "+Avl_File_Count);
			SkySiteUtils.waitTill(10000);
			//Select first two files from the list
			if(Avl_File_Count>=2)
			{
				for(int i=1;i<=2;i++)
				{
					//Log.message("Select a checkbox");
					driver.findElement(By.xpath("(//input[@type='checkbox' and @data-bind='click: ItemChkboxClick, clickBubble: false, checked: IsChecked'])["+i+"]")).click();
				}
			}
			SkySiteUtils.waitTill(5000);
			
			//String Msg_No_Of_FilesSel = msgforselectedfiles.getText();
			btnDropdownMultPhotoSel.click();
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("//i[@class='icon icon-ellipsis-horizontal media-object icon-lg' and @data-placement='left']/../following-sibling::ul/li[2]//span")).click();
			Log.message("Clicked on Send Link Tab for the Photos.");
			SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
			Log.message("Waiting for send link window");
			SkySiteUtils.waitTill(5000);
			String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");//Copy the URL
			Log.message("Generated Send Link is: "+Act_SendLinkURL);
			btnContact.click();
			Log.message("Clicked on contact-address book.");
			SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
			Log.message("Waiting for Select Contact window");
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();//Select 1st contact check box
			btnSelectContact.click();//Click on select button
			SkySiteUtils.waitTill(5000);
			txtMessage.click();
			txtMessage.sendKeys("Testing from QA_Team @Automation");
			btnSendFileView.click();//Click on send
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			Log.message("Waiting for notify message could display");
			String NotMsg_SendLink = notificationMsg.getText();
			SkySiteUtils.waitTill(5000);
			iconGridView.click();//Click on grid view
			SkySiteUtils.waitTill(5000);
			if(NotMsg_SendLink.contentEquals("Link successfully sent"))
			{
				Log.message("Multiple photo select and sendlink is done successfully");
				SkySiteUtils.waitTill(10000);
				
				driver.get(Act_SendLinkURL);//Launch the URL copied from Clip board
				SkySiteUtils.waitTill(5000);
				SkySiteUtils.waitForElement(driver, btnSingFileDownload, 50);
				Log.message("Waiting for download button to be appeared from sendlink URL");
				
				//Single photo download from send link url
				String Exp_File_Name = driver.findElement(By.xpath("(//div[1]/h4)[2]")).getText();		
				Log.message("First Photo from url is: "+Exp_File_Name);
				result1 = this.Download_SingleFile_SendLinkForAFIle(Exp_File_Name);//Validating download a photo from URL
				
				//Total Zip download from send link url
				String Exp_Zip_Name = "2-photos-"+Foldername+"-"+Project_Name;
				Log.message("Expected Zip is: "+Exp_Zip_Name);
				result2 = this.Download_ZipFile_SendLinkForMultFIle(Exp_Zip_Name);//Validate total zip download
				
				//Select more than one photo and download the zip
				result3 = this.Download_ZipFile_SendLinkBySelectMultFIle(Exp_Zip_Name);//Validate multiple photo zip download
					
			}
		
		if((result1==true)&&(result2==true)&&(result3==true))
		{
			Log.message("Multiple photo send link and all types of downloads from url is working.");
			return true;
		}
		else
		{
			Log.message("Multiple photo send link and all types of downloads from url is NOT working.");
			return false;
		}
		
	}
	
	
	/** 
	 * Method written for Validate Download functionality by select multiple photos
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	//Validate Send link by select multiple files and download
	public boolean ValidateDownload_BySelectMultPhotos(String Foldername,String Project_Name) throws InterruptedException, AWTException
	{		
			boolean result = false;
						
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, iconListView, 30);
			Log.message("Waiting for List View button to be appeared");
			SkySiteUtils.waitTill(5000);
			iconListView.click();
			Log.message("Clicked on List View button.");
			SkySiteUtils.waitForElement(driver, chkboxAllPhoto, 30);
			Log.message("Waiting for select all photos checkbox to be appeared");
			//Counting number of files available
			int Avl_File_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'media navbar-collapse pw-photo')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_File_Count = Avl_File_Count+1; 	
			} 
			Log.message("Availble number of photos in folder is: "+Avl_File_Count);
			SkySiteUtils.waitTill(10000);
			//Select first two files from the list
			if(Avl_File_Count>=2)
			{
				for(int i=1;i<=2;i++)
				{
					//Log.message("Select a checkbox");
					driver.findElement(By.xpath("(//input[@type='checkbox' and @data-bind='click: ItemChkboxClick, clickBubble: false, checked: IsChecked'])["+i+"]")).click();
				}
			}
			SkySiteUtils.waitTill(5000);
			
			//Calling "Deleting download folder files" method
			String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
			Log.message("Download Path is: "+Sys_Download_Path);
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
			SkySiteUtils.waitTill(10000);
			
			btnDropdownMultPhotoSel.click();
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("//i[@class='icon icon-ellipsis-horizontal media-object icon-lg' and @data-placement='left']/../following-sibling::ul/li[1]//span")).click();//Click on Download tab
			Log.message("Clicked on Download Tab for the Photos.");
			SkySiteUtils.waitTill(40000);
			
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(60000);
			}
			
			//After Download, checking whether Downloaded file is existed under download folder or not 
			String ActualFilename=null;
			File[] files = new File(Sys_Download_Path).listFiles();
						
			for(File file : files)
			{
				if(file.isFile()) 
				{
					ActualFilename=file.getName();//Getting File Names into a variable
					Log.message("Actual File name is:"+ActualFilename);
					SkySiteUtils.waitTill(1000);	
					Long ActualFileSize=file.length();
					Log.message("Actual File size is:"+ActualFileSize);
					String Exp_Zip_Name = "2-photos-"+Foldername+"-"+Project_Name;
					Log.message("Expected Zip is: "+Exp_Zip_Name);
					if((ActualFilename.contains(Exp_Zip_Name))&&(ActualFileSize!=0))						
					{
						Log.message("Downloaded ZIP file is available in downloads!!!");
						result = true;
						break;
					}
								
				}
							
			}
			
			iconGridView.click();//Click on grid view
			SkySiteUtils.waitTill(5000);	
			
		if(result==true)
		{
			Log.message("Multiple photo select and download is working.");
			return true;
		}
		else
		{
			Log.message("Multiple photo select and download is NOT working.");
			return false;
		}
		
	}
	
	/** 
	 * Method written for Validate Folder send link and download sub Folder
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	//Validate Send link by select multiple files and download
	public boolean ValidateDownload_SubFolder_FromFolderSendLink(String Foldername) throws InterruptedException, AWTException
	{		
			boolean result1 = false;
			boolean result2 = false;
			boolean result3 = false;
			boolean result4 = false;
			
			SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
			Log.message("Waiting for Create Folder button to be appeared");
		//Getting Folder count after created a new folder
			int Avl_Fold_Count=0;
			String actualFolderName = null;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_Fold_Count = Avl_Fold_Count+1; 	
			} 
			Log.message("Available Folder Count is: "+Avl_Fold_Count);
			
			int j=1;
			for(j=1;j<=Avl_Fold_Count;j++)
			{
				actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
				Log.message("Exp Name:"+Foldername);
				Log.message("Act Name:"+actualFolderName);
		
			//Validating the new Folder is created or not
				if(Foldername.trim().contentEquals(actualFolderName.trim()))
				{  	
					result1 = true;
					Log.message("Expected Folder is Availble with name: "+Foldername);
					break;
				}
			}
			
			if(result1==true)
			{
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])["+j+"]")).click();//Folder Dropdown
				Log.message("Clicked on More options icon for the Folder.");
				SkySiteUtils.waitTill(2000);
				WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Send')])["+j+"]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click send
				Log.message("Clicked on Send Link Tab for the Folder.");
				SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
				Log.message("Waiting for send link window");
				SkySiteUtils.waitTill(5000);
				String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");//Copy the URL
				Log.message("Generated Send Link is: "+Act_SendLinkURL);
				btnContact.click();
				Log.message("Clicked on contact-address book.");
				SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
				Log.message("Waiting for Select Contact window");
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();//Select 1st contact check box
				btnSelectContact.click();//Click on select button
				SkySiteUtils.waitTill(5000);
				txtMessage.click();
				txtMessage.sendKeys("Testing from QA_Team @Automation");
				btnSendFileView.click();//Click on send
				SkySiteUtils.waitForElement(driver, notificationMsg, 30);
				Log.message("Waiting for notify message could display");
				String NotMsg_SendLink = notificationMsg.getText();
				SkySiteUtils.waitTill(5000);
				if(NotMsg_SendLink.contentEquals("Link successfully sent"))
				{
					Log.message("Multiple file select and sendlink is done successfully");
					SkySiteUtils.waitTill(10000);
					
					driver.get(Act_SendLinkURL);//Launch the URL copied from Clip board
					SkySiteUtils.waitTill(5000);
					SkySiteUtils.waitForElement(driver, btnSingFileDownload, 50);
					Log.message("Waiting for download button to be appeared from sendlink URL");
					
					//Sub Folder download from send link url
					String Exp_SubFold_Name = eleSubFoldURL.getText();		
					Log.message("Sub Folder from url is: "+Exp_SubFold_Name);
					result2 = this.Download_SubFolder_FoldSendLink(Exp_SubFold_Name);//Validating download a Sub Folder from URL
									
					eleSubFoldURL.click();
					Log.message("Clicked on subfolder.");
					SkySiteUtils.waitForElement(driver, btnBackFold, 30);
					Log.message("Waiting for back button to be appeared");
					
					//Single File download from send link url
					String Exp_File_Name = driver.findElement(By.xpath(".//*[@id='sharedDocsFolderList']/li[1]/div/div[1]/h4")).getText();		
					Log.message("First File from url is: "+Exp_File_Name);
					result3 = this.Download_SingleFile_SendLinkSubFold(Exp_File_Name);//Validating download a file from URL
					
					SkySiteUtils.waitTill(5000);
					btnBackFold.click();
					SkySiteUtils.waitTill(5000);
					
					//Total Zip download from send link url
					result4 = this.Download_ZipFile_SendLinkForMultFIle(Foldername);//Validate total zip download
			
				}
	
			}
				
		if((result1==true)&&(result2==true)&&(result3==true)&&(result4==true))
		{
			Log.message("Folder having subfolder send link and all types of downloads from url is working.");
			return true;
		}
		else
		{
			Log.message("Folder having subfolder send link and all types of downloads from url is NOT working.");
			return false;
		}
		
	}
	
	/** 
	 * Method written for Validate File download from revision history
	 *  Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	//Validate Send link by select multiple files and download
	public boolean ValidateDownload_File_FromRevisionHistory() throws InterruptedException, AWTException
	{		
			boolean result1 = false;
			
			SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
			Log.message("Waiting for Create Folder button to be appeared");
			SkySiteUtils.waitTill(5000);
			//Get The Count of available files in selected folder
			int Avl_File_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]")); 
			for (WebElement Element : allElements) 
			{ 
				Avl_File_Count = Avl_File_Count+1; 	
			} 
			Log.message("Available File Count is: "+Avl_File_Count);
			
			if(Avl_File_Count>=1)
			{
				String FirstFileName = driver.findElement(By.xpath("(//span[@class='h4_docname document-preview-event'])[1]")).getText();//First file name
				Log.message("Act Name:"+FirstFileName);
				
				driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[1]")).click();//Click on file drop list
				SkySiteUtils.waitTill(2000);
				
				WebElement RevisionLink = driver.findElement(By.xpath("(//a[contains(text(),'Revision history')])[1]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",RevisionLink);//JSExecutor
				Log.message("Clicked on Revision history tab of a file.");			
				
				//Calling "Deleting download folder files" method
				String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
				Log.message("Download Path is: "+Sys_Download_Path);
				projectDashboardPage = new ProjectDashboardPage(driver).get();
				projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
				SkySiteUtils.waitTill(10000);
				
				driver.findElement(By.xpath("(//i[@class='icon icon-download-alt icon-lg'])[3]")).click();//Click on download icon
				SkySiteUtils.waitTill(15000);
				Log.message("Clicked on download icon from revision history.");
				
				//Get Browser name on run-time.
				Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
				String browserName = caps.getBrowserName();
				Log.message("Browser name on run-time is: "+browserName);
				
				if(browserName.contains("firefox"))
				{
				//Handling Download PopUp of firefox browser using robot
					Robot robot=null;
					robot=new Robot();
					
					robot.keyPress(KeyEvent.VK_ALT);
					SkySiteUtils.waitTill(2000);
					robot.keyPress(KeyEvent.VK_S);
					SkySiteUtils.waitTill(2000);
					robot.keyRelease(KeyEvent.VK_ALT);
					robot.keyRelease(KeyEvent.VK_S);
					SkySiteUtils.waitTill(3000);
					robot.keyPress(KeyEvent.VK_ENTER);
					robot.keyRelease(KeyEvent.VK_ENTER);
					SkySiteUtils.waitTill(30000);
				}
				
				driver.findElement(By.xpath("//button[@class='close']")).click();//Click on close revision history window
				SkySiteUtils.waitTill(5000);
				Log.message("Clicked on close button of revision window.");
				
				//After Download, checking whether Downloaded file is existed under download folder or not 
				String ActualFilename=null;
				File[] files = new File(Sys_Download_Path).listFiles();
							
				for(File file : files)
				{
					if(file.isFile()) 
					{
						ActualFilename=file.getName();//Getting File Names into a variable
						Log.message("Actual File name is:"+ActualFilename);
						SkySiteUtils.waitTill(1000);	
						Long ActualFileSize=file.length();
						Log.message("Actual File size is:"+ActualFileSize);
						if((ActualFilename.contains(FirstFileName))&&(ActualFileSize!=0))						
						{
							Log.message("Downloaded file is available in downloads!!!");
							result1 = true;
							break;
						}
									
					}
								
				}
				
			}
			
			if((result1==true))
			{
				Log.message("File download from revision history window is working.");
				return true;
			}
			else
			{
				Log.message("File download from revision history window is NOT working.");
				return false;
			}
	
	}
	
	/** 
	 * Method written for Move a folder to a destination folder
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	@FindBy(css="#btnCancelMoveFolder")
	WebElement btnCancelMoveFolder;
	
	@FindBy(css="#btnMoveFolder")
	WebElement btnSaveMoveFolder;
	
	@FindBy(css="#button-0")
	WebElement btnInfoMsgNo;
	
	@FindBy(css="#button-1")
	WebElement btnInfoMsgYes;
	
	public boolean Move_Folder_ToDestinationFolder(String Source_Folder, String Desti_Folder) throws InterruptedException, AWTException, IOException
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		boolean result4 = false;
		
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
	//Getting Folder count after created a new folder
		int Avl_Fold_Count=0;
		int j=0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Fold_Count = Avl_Fold_Count+1; 	
		} 
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		
		for(j=1;j<=Avl_Fold_Count;j++)
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Exp Name:"+Source_Folder);
			Log.message("Act Name:"+actualFolderName);
	
		//Validating the new Folder is created or not
			if(Source_Folder.trim().contentEquals(actualFolderName.trim()))
			{  	
				result1 = true;
				Log.message("Expected Source Folder is available with name: "+Source_Folder);
				break;
			}
		}
		
		int i = 0;
		if(result1==true)
		{
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+j+"]/section[4]/ul/li[4]/a/i")).click();//Click on Source folder droplist
			Log.message("Clicked on drop list of source folder.");
			SkySiteUtils.waitTill(3000);
			int MoveButPlace = j-1;
			WebElement MoveTab = driver.findElement(By.xpath("(//a[contains(text(),'Move')])["+MoveButPlace+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",MoveTab);//JSExecutor to click Move
			Log.message("Clicked on Move Tab for the source folder.");
			SkySiteUtils.waitForElement(driver, btnCancelMoveFolder, 30);
			Log.message("Waiting for Move Folder Window cancel button to be appeared");
			SkySiteUtils.waitTill(15000);
			
			//Select a destination folder from the folder tree of move folder window
			for(i=2;i<=Avl_Fold_Count;i++)
			{
				String Act_DestFolder = driver.findElement(By.xpath("(//span[@class='standartTreeRow'])["+i+"]")).getText();//Getting Folder Name
				Log.message("Actual Destination Folder is: "+Act_DestFolder);
				if(Act_DestFolder.equalsIgnoreCase(Desti_Folder))
				{
					result2 = true;
					Log.message("Expected destination folder is available under Folder tree.");
					break;
				}	
			}
			
			if(result2==true)
			{
				driver.findElement(By.xpath("(//span[@class='standartTreeRow'])["+i+"]")).click();//Select Destination folder
				Log.message("clicked on Expected destination folder.");
				SkySiteUtils.waitTill(5000);
				btnSaveMoveFolder.click();
				Log.message("clicked on Save button.");
				SkySiteUtils.waitForElement(driver, btnInfoMsgNo, 30);
				Log.message("Waiting for Conformation window No button to be appeared");
				SkySiteUtils.waitTill(2000);
				btnInfoMsgYes.click();
				Log.message("clicked on Yes button for the conformation message.");
				SkySiteUtils.waitTill(15000);
				//SkySiteUtils.waitForElement(driver, notificationMsg, 30);
				//String MoveFold_NotifyConfMsg = notificationMsg.getText();
				//Log.message("Notify message after move a folder is: "+MoveFold_NotifyConfMsg);
				for(j=1;j<=Avl_Fold_Count-1;j++)
				{
					actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
					Log.message("Exp Name:"+Desti_Folder);
					Log.message("Act Name:"+actualFolderName);
			
				//Validating the new Folder is created or not
					if(Desti_Folder.trim().contentEquals(actualFolderName.trim()))
					{  	
						result3 = true;
						Log.message("Expected Destination Folder is available with name: "+Desti_Folder);
						break;
					}
				}				
				
				if(result3==true)
				{
					driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).click();//Select Dest Folder
					Log.message("Clicked on Destination folder.");
					SkySiteUtils.waitTill(10000);
					int Folders_Under_DestFold=0;
					List<WebElement> allElements1 = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
					for (WebElement Element : allElements1) 
					{ 
						Folders_Under_DestFold = Folders_Under_DestFold+1; 	
					} 
					Log.message("Source Folder is Available under destination folder is: "+Folders_Under_DestFold);
					
					for(j=2;j<=Folders_Under_DestFold+1;j++)
					{													
						actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
						Log.message("Exp Name:"+Source_Folder);
						Log.message("Act Name:"+actualFolderName);
				
					//Validating the new Folder is created or not
						if(Source_Folder.trim().contentEquals(actualFolderName.trim()))
						{  	
							result4 = true;
							Log.message("Source Folder is available under destination folder: "+Source_Folder);
							driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();//Click on project name from breadcrumb
							Log.message("Clicked on project name from breadcrumb to navigate back to folder level.");
							SkySiteUtils.waitTill(10000);
							break;
						}
					}
					
				}
					
					
			}
				
		}		
		
		//Final Validation
		if((result1==true)&&(result2==true)&&(result3==true)&&(result4==true))
		{
			Log.message("Folder moved to destination folder successfully!!!");
			return true;
		}
		else
		{
			Log.message("Folder moved to destination folder FAILED!!!");
			return false;
		}
			
	}
	
	/** 
	 * Method written for Download a folder
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean Download_Folder(String Folder_Name) throws InterruptedException, AWTException
	{
		boolean result1=false;
		boolean result2=false;
		SkySiteUtils.waitTill(5000);
		//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
		
	//Getting Folder count after created a new folder
		int Avl_Fold_Count=0;
		int j=0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Fold_Count = Avl_Fold_Count+1; 	
		} 
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		
		for(j=1;j<=Avl_Fold_Count;j++)
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Exp Name:"+Folder_Name);
			Log.message("Act Name:"+actualFolderName);
	
		//Validating the new Folder is created or not
			if(Folder_Name.trim().contentEquals(actualFolderName.trim()))
			{  	
				result1 = true;
				Log.message("Expected Folder is available with name: "+Folder_Name);
				break;
			}
		}
		
		if(result1==true)
		{
			//Going to download destination folder
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+j+"]/section[4]/ul/li[4]/a/i")).click();//Click on Source folder droplist
			Log.message("Clicked on drop list of expected folder.");
			SkySiteUtils.waitTill(3000);
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Download document')])[4]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click Download
			Log.message("Clicked on Download Tab for the expected folder.");
			SkySiteUtils.waitTill(40000);
			
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(40000);
			}
			
			//After Download, checking whether Downloaded file is existed under download folder or not 
			String ActualFilename=null;
			File[] files = new File(Sys_Download_Path).listFiles();
						
			for(File file : files)
			{
				if(file.isFile()) 
				{
					ActualFilename=file.getName();//Getting File Names into a variable
					Log.message("Actual File name is:"+ActualFilename);
					SkySiteUtils.waitTill(1000);	
					Long ActualFileSize=file.length();
					Log.message("Actual File size is:"+ActualFileSize);
					if((ActualFilename.contains(Folder_Name))&&(ActualFileSize!=0))						
					{
						Log.message("Downloaded Folder is available in System downloads!!!");
						result2 = true;
						break;
					}
								
				}
							
			}
						
		}		
		if((result1==true)&&(result2==true))
			return true;
		else
			return false;
	
	}
	
	/** 
	 * Method written for Send Link of Download a folder
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean Download_Folder_ValidateInsideZip(String Folder_Name,String FolderPath,int FileCount) throws InterruptedException, AWTException, IOException
	{
		boolean result1=false;
		boolean result2=false;
		boolean result3=false;
		
		SkySiteUtils.waitTill(5000);
		//Calling "Deleting download folder files" method
		String Sys_Download_Path = PropertyReader.getProperty("SysDownloadPath");
		Log.message("Download Path is: "+Sys_Download_Path);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
		
	//Getting Folder count after created a new folder
		int Avl_Fold_Count=0;
		int j=0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Fold_Count = Avl_Fold_Count+1; 	
		} 
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		
		for(j=1;j<=Avl_Fold_Count;j++)
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			//Log.message("Exp Name:"+Folder_Name);
			//Log.message("Act Name:"+actualFolderName);
	
		//Validating the new Folder is created or not
			if(Folder_Name.trim().contentEquals(actualFolderName.trim()))
			{  	
				result1 = true;
				Log.message("Expected Folder is available with name: "+Folder_Name);
				break;
			}
		}
		
		if(result1==true)
		{
			//Going to download destination folder
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+j+"]/section[4]/ul/li[4]/a/i")).click();//Click on Source folder droplist
			Log.message("Clicked on drop list of expected folder.");
			SkySiteUtils.waitTill(3000);
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Download document')])[4]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click Download
			Log.message("Clicked on Download Tab for the expected folder.");
			SkySiteUtils.waitTill(120000);
			
			//Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: "+browserName);
			
			if(browserName.contains("firefox"))
			{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
				
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(40000);
			}
			
			//After Download, checking whether Downloaded file is existed under download folder or not 
			String ActualFilename=null;
			File[] files = new File(Sys_Download_Path).listFiles();
						
			for(File file : files)
			{
				if(file.isFile()) 
				{
					ActualFilename=file.getName();//Getting File Names into a variable
					//Log.message("Actual File name is:"+ActualFilename);
					SkySiteUtils.waitTill(1000);	
					Long ActualFileSize=file.length();
					//Log.message("Actual File size is:"+ActualFileSize);
					if((ActualFilename.contains(Folder_Name))&&(ActualFileSize!=0))						
					{
						Log.message("Downloaded Folder is available in System downloads!!!");
						result2 = true;
						break;
					}
								
				}
							
			}
			
			if(result2==true)
			{
				result3 = this.Validate_Files_FromZipFolder(Sys_Download_Path, ActualFilename, FolderPath, FileCount);
			}
						
		}
		
		if((result1==true)&&(result2==true)&&(result3==true))
			return true;
		else
			return false;
	
	}
	
	/** 
	 * Method written for Validate files from ZIP folder
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	//Validate Files are available in ZIP or not
	public boolean Validate_Files_FromZipFolder(String Sys_Download_Path,String ActualFilename,String FolderPath,int FileCount) throws InterruptedException
	{	
		boolean result = false;
		try
		{	
			//Zip File validation
			String ZIP_FILE_NAME = Sys_Download_Path+"\\"+ActualFilename;
			final ZipFile file = new ZipFile(ZIP_FILE_NAME); 
			Log.message("Iterating over zip file is : " + ZIP_FILE_NAME);
			int uploadSuccessCount=0;
			final Enumeration<? extends ZipEntry> entries = file.entries(); 
			while (entries.hasMoreElements()) 
			{ 
				final ZipEntry entry = entries.nextElement(); 
				String FName_FrmZip = entry.getName();
				Log.message("File Name from Zip folder is: "+entry.getName());
				long FileSize_FrmZip = entry.getSize();
				//Log.message("Size of a file from Zip Folder is: "+entry.getSize());
				//System.out.printf("File: %s Size %d %n", entry.getName(), entry.getSize()); 
				//extractEntry(entry, file.getInputStream(entry));
				
				//Reading all the file names from input folder
				File[] files = new File(FolderPath).listFiles();
				for(File file1 : files)
				{
					if(file1.isFile()) 
					{
						String expFilename=file1.getName();//Getting File Names into a variable
						//Log.message("Expected File name is:"+expFilename);
						SkySiteUtils.waitTill(500);
						     
						if((FName_FrmZip.contains(expFilename)) && (FileSize_FrmZip!=0))
						{	    			    	 
							uploadSuccessCount=uploadSuccessCount+1;
							Log.message("File : " +expFilename+" is available in ZIP Folder then Sucess Count:"+uploadSuccessCount);
							break;
						}
					}
				     		     
				}
						     
			}

			//Checking whether file count is equal or not		     
			if(FileCount==uploadSuccessCount)	
			{
				Log.message("All the files are available in ZIP folder.");
				result=true;
			}
			else
			{	
				Log.message("All the files are NOT available in ZIP folder.");
				result=false;
			}
			
		}//End of try block
			
		catch(Exception e)
		{	
			result=false;	
		}
			
		finally
		{
				return result;	
		}
	}

	
	
}
