package com.arc.Common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

import com.steadystate.css.parser.ParseException;

public class Generate_Random_Number 
{
	
	public static String generateRandomValue()
	{
		Random r = new Random( System.currentTimeMillis() );
		return String.valueOf((100000 + r.nextInt(200000)));
	}
	
	
	public static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
	
	public static String RandomEmail() throws ParseException, java.text.ParseException {
		String Email = null;

		Email = "IndiaArc" + "." + getRandomNumberInRange(0000, 1210)+"@" + "Disposible.com" ;
		System.out.println(Email);
		return Email;
	}
	


	public static String getRandomDateOfBirth() throws ParseException, java.text.ParseException {
		String Dob = null;

		Dob = getRandomNumberInRange(1, 28) + "/" + getRandomNumberInRange(1, 12) + "/"
				+ getRandomNumberInRange(1990, 2010);

		return formatDate(Dob);
	}
	
	public static String ProjectName() throws ParseException, java.text.ParseException {
		String ProjectName = null;

		ProjectName = "ARC_Document_Solutions" + "/" + getRandomNumberInRange(1, 1210) ;
		return ProjectName;
	}
	

	public static String formatDate(String dateFormat) throws ParseException, java.text.ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = sdf.parse(dateFormat);
		String st = sdf.format(date);
		return st;
	}

	public static String getRandomMobileNo() {
		String mobile;
		mobile = getRandomNumberInRange(98111, 99999) + "" + getRandomNumberInRange(11111, 99999);
		return mobile;
	}

	public static String getRandomText(int length) {
		return RandomStringUtils.randomAlphabetic(length);
	}

	public static String getCurrentDate() {
		Date date = new Date();
		SimpleDateFormat dt1 = new SimpleDateFormat("ddMMyy");
		//System.out.println(dt1.format(date));
		return dt1.format(date);
		
	}

}
